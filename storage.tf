#Define providers used
provider "azurerm" {
  version         = "=2.11"
  tenant_id       = "${var.tenant}"
  subscription_id = "${var.subscription}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  features {} #This is required for v2 of the provider even if empty or plan will fail
}

variable "tenant" {
  type = string
}
variable "subscription" {
  type = string
}
variable "client_id" {
  type = string
}
variable "client_secret" {
  type = string
}

#Data section
data "azurerm_resource_group" "ResGroup" {
  name = "terraform-azure-automation"
}

#Resource section
resource "azurerm_storage_account" "terraform-azure-automation" {
  name                     = "storagesreactapp"
  resource_group_name      = data.azurerm_resource_group.ResGroup.name
  location                 = data.azurerm_resource_group.ResGroup.location
  account_kind             = "StorageV2"
  account_tier             = "Standard"
  account_replication_type = var.replicationType

  static_website {
    index_document = "index.html"
  }
}

resource "azurerm_storage_container" "Container1" {
  name                  = "react-app"
  storage_account_name  = azurerm_storage_account.terraform-azure-automation.name
  container_access_type = "private"
}
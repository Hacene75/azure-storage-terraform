#### Create azure resource storage account using terraform

- download terraform from https://www.terraform.io/downloads.html
- Create the tf files
- terraform fmt  # make my files formatted correctly and will fix all tf files in this folder 
- terraform init : initialize terraform, This command performs several different initialization steps in order to prepare a working directory for use.
- install az login cli to connect to azure portal
- terraform plan 


terraform apply -auto-approve

##### Human readable view of the state file
terraform show

##### Show specific resource from state file
terraform state list
terraform state show azurerm_storage_account.StorAccount1
terraform state show azurerm_storage_container.Container1

terraform plan -var 'replicationType=GRS'
terraform apply -var 'replicationType=GRS' -auto-approve

##### To visually see
terraform graph > base.dot

##### could sent directly with graphviz installed https://graphviz.gitlab.io/download/
terraform graph | dot -Tsvg > graph.svg

##### If resources changed outside of terraform and state not current
terraform refresh

##### For secrets
Set-Location $GitBasePath\SecretOnly

##### Set variable to avoid having in my source files. Could also use terraform.tfvars etc
$env:TF_VAR_KV="/subscriptions/$SubID/resourceGroups/RG-SCUSA/providers/Microsoft.KeyVault/vaults/SavillVault"

##### To delete the resources
terraform plan -destroy -out='planout'   #Is there a file type to use? .tfplan??
terraform apply 'planout'

##### or
terraform destroy


#### Useful links

https://medium.com/@moonape1226/how-to-integrated-terraform-plan-in-gitlab-ci-with-azure-infrastructure-b87814a7a093
https://github.com/johnthebrit/RandomStuff/blob/master/TerraformwithAzure/IntroBasicDeclarative/DemoScript.ps1
https://samcogan.com/deploying-arm-templates-with-terraform/
https://medium.com/faun/automating-your-deployment-using-gitlab-azure-storage-static-website-hosting-75c767b2569f
https://medium.com/@antbutcher89/hosting-a-react-js-app-on-azure-blob-storage-azure-cdn-for-ssl-and-routing-8fdf4a48feeb
https://medium.com/@mutebg/using-gitlab-to-build-test-and-deploy-modern-front-end-applications-bc940501a1f6
https://medium.com/@mutebg/using-gitlab-to-build-test-and-deploy-modern-front-end-applications-bc940501a1f6